const getSum = (str1, str2) => {
    if (str1 === '') {
        str1 = 0;
    }

    if (str2 === '') {
        str2 = 0;
    }

    if (isNaN(str1) || isNaN(str2) || typeof str1 === "object" || typeof str2 === "object") {
        return false;
    }

    return (+str1 + +str2).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
    let postCount = 0;
    let commentCount = 0;

    for (const post of listOfPosts) {
        if (post.author === authorName) {
            postCount++;
        }

        if (post.hasOwnProperty("comments")) {

            for (const obj of post.comments) {
                if (obj.author === authorName) {
                    commentCount++;
                }
            }
        }
    }

    return `Post:${postCount},comments:${commentCount}`;
};

const tickets = (people) => {
    bills = [0, 0, 0];

    for (const bill of people) {
        if (bill === 25) {
            bills[0] += 1;
        }

        if (bill === 50) {
            bills[0] -= 1;
            bills[1] += 1;
        }

        if (bill === 100 && bills[1] === 0) {
            bills[0] -= 3;
        }

        if (bill === 100 && bills[1] > 0) {
            bills[0] -= 1;
            bills[1] -= 1;
        }


    }
    return !(bills[0] < 0 || bills[1] < 0 || bills[2] < 0) ? "YES" : "NO";
};


module.exports = { getSum, getQuantityPostsByAuthor, tickets };
